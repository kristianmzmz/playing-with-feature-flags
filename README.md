# Set up

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Configuration
You will need a .env file containing the url, secret and environment for the feature flags. 
If you use gitlab yoy can find this valies under Deployment -> Feature Flags -> Configuration

```
VUE_APP_ENV=development
VUE_APP_FF_HOST=https://gitlab.com/api/v4/feature_flags/unleash/XXXXX
VUE_APP_FF_INSTANCEID=XXXXXXXXXXXXXXX
```


# About the repo
This repository is the final state of the talk about "Continuous Delivery in VUE using Feature Flags"
It contains difrerent states of the app that will be explained during the presentation.

The talk slides can be found here in [english](https://docs.google.com/presentation/d/1V_TxKrKp2uP_rcXPiqEgoNqj_TBkLd-Qh2sIrye1HfU/edit#slide=id.g77ec769f6d_2_216) and in [spanish](https://docs.google.com/presentation/d/1_p0HMmNkC3njwE5n_6N9t3rgv7sndnSRBr81SgpYA68/edit?usp=sharing)
