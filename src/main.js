import Vue from 'vue'
import App from './App.vue'
import GitLabFF from '../plugins/gitlab-ff-plugin'
import UnleashPlugin from '../plugins/vue-unleash/index'
import UserWithIdStrategy from '../plugins/vue-unleash/user-with-id'
import Vuex from 'vuex'

Vue.use(Vuex);
const store = new Vuex.Store({});

Vue.use(GitLabFF)
Vue.use(UnleashPlugin, {
  appName: 'featureFlags',
  instanceId: process.env.VUE_APP_FF_INSTANCEID,
  host: process.env.VUE_APP_FF_HOST,
  store,
  strategyProviders: {
    userWithId: UserWithIdStrategy
  }
})
Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App),
}).$mount('#app')

