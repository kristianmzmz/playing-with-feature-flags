import { shallowMount } from "@vue/test-utils"
import Loader from '@/components/Loader'

describe('Loader', () => {
    it('Should be displayed when is loading', () => {
        const loader = shallowMount(Loader, {
            propsData: {
                loading: true
            }
        })

        expect(loader.text()).toContain('Loading NASA results...')
    })
    it('Should be hidden if loading is false', () => {
        const loader = shallowMount(Loader, {
            propsData: {
                loading: false
            }
        })

        expect(loader.text()).toEqual('')
    })
})
