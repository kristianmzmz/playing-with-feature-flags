import { mount } from '@vue/test-utils'
import Search from '@/components/Search'
import axios from 'axios'
import mockedApiResponse from '../mocks/MockedAPI'

let searchComponent;

describe('', () => {
  beforeEach(() =>{
    searchComponent = mount(Search)
  })

  it('A results string is rendered', () => {
    expect(searchComponent.text()).toContain('Found images(0)')
  })

  it('Should render the provided data value', async () => {
    const currentNumberOfItems = 23

    searchComponent.setData({
      numberOfImages: currentNumberOfItems
    })
    await searchComponent.vm.$nextTick()

    expect(searchComponent.text()).toContain('Found images(' + currentNumberOfItems + ')')
  })
})

describe('When a call is made to the API ', () => {

  beforeEach(() => {
    axios.get =jest.fn(() => Promise.resolve(mockedApiResponse));
    searchComponent = mount(Search)
  })

  it('Should call the API on submit', async () => {
    const query = "sun"

    searchComponent.setData({ query })
    searchComponent.find('form').trigger('submit')
    await searchComponent.vm.$nextTick()

    expect(axios.get).toBeCalledWith(
      'https://images-api.nasa.gov/search',
      {
        params: {
          media_type: "image",
          q: query
        }
      }
    )
  })

  it('Should call the API on submit and update the results array', async () => {
    jest.useFakeTimers()

    const query = 'sun'
    searchComponent.setData({ query })
    searchComponent.find('form').trigger('submit')

    await searchComponent.vm.$nextTick()
    jest.runAllTimers()

    expect(searchComponent.vm.numberOfImages).toBe(2)
  })

  it('Should display loading while waiting for API answer', async () => {
    searchComponent.setData({ isLoading: true })

    await searchComponent.vm.$nextTick()

    expect(searchComponent.text()).toContain('Loading NASA results...')
  })
});

