// eslint-disable-next-line no-unused-vars
let mockedApiResponse;

export default mockedApiResponse = {
  data: {
    collection: {
      items: [
        {
          links: [
            {
              href: 'http://www.this-is-a-link-com'
            }
          ]
        },
        {
          links: [
            {
              href: 'http://www.this-is-another-link-com'
            }
          ]
        }
      ]
    }
  }
}
