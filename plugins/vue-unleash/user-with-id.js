export default function(parameters) {
    const userIds =  parameters.userIds ? parameters.userIds.split(',') : []

    // Let's assume is the Logged in user email
    return userIds.find(userId => userId === 'kristian.munoz@codurance.com')
}
