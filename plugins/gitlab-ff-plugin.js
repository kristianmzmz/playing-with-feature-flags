import axios from 'axios'
const host = process.env.VUE_APP_FF_HOST
const instanceId = process.env.VUE_APP_FF_INSTANCEID

const headers = {
    'UNLEASH-INSTANCEID': instanceId,
    'UNLEASH-APPNAME': process.env.VUE_APP_ENV
}

let allFeatureFlags = [];

async function fetchAll() {
    try {
        allFeatureFlags = await axios
            .get(`${host}/features`, {headers})
            .then(response => {
                if(!response.data.features.length){
                    throw new Error('No feature flags found')
                }

                return response.data.features
            })
    } catch (e) {
        console.error('There was an error:', e)
    }
}

async function featureIsEnabled(featureName) {
    try {
        if(!allFeatureFlags.length){
            await fetchAll()
        }

        const featureFlag = allFeatureFlags.find(featureFlag => featureFlag.name === featureName)
        return featureFlag?.enabled
    } catch (e) {
        console.error('There was an error:', e)
        return false
    }
}

export default {
    // eslint-disable-next-line no-unused-vars
    install(Vue, options) {
        Vue.prototype.$featureIsEnabled = featureIsEnabled
    }
}
